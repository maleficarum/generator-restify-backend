let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index');

const URL = "http://localhost:<%= httpPort %>";

chai.use(chaiHttp);

describe('Test components', () => {

    it('Should say hello', (done) => {
        chai.request(URL).post('/hello').set('Content-header','application/json').send({"name":"Client"}).end((err, res) => {
        
            if(err) {
                console.error(err);
            }
            
            res.should.have.status(200);

            done();
        });
    });

});