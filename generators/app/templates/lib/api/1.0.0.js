"use strict";

const VERSION = "1.0.0";
const common = require('../common.js');

var log = common.getLogger('api-' + VERSION);

const self = module.exports = {
    version: () => {
        return VERSION;
    },
    root: (req, res, next) => {
        res.send({ version: VERSION});
        return next();
    },
    hello: (req, res, next) => {
        res.send({
            "message": "Hello " + req.body.name ? req.body.name : "World!"
        });
        return next();
    }
};