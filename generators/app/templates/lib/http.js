"use strict";

const cors = require('cors');
const helmet = require('helmet');
const hide = require('hide-powered-by');
const restify = require('restify');
const walk = require('walk-sync');
const fs = require('fs');
const common = require('./common.js');

const MODULE_NAME = "http-rest-server";

var config;
var log;
var server;
var apis = [];

const self = module.exports = {
    config: (__config__) => {
        config = __config__;

        log = common.getLogger(MODULE_NAME);

        server = restify.createServer();

        server.opts("/.*/", function(req, res, next) {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Methods', "GET, POST");
            res.header('Access-Control-Allow-Headers', req.header('Access-Control-Request-Headers'));
            res.send(200);
            return next();
        });
    
        server.use(cors({
            "origin": "*",
            "methods": "GET,HEAD,PUT,PATCH,POST,DELETE"
        }));      
    
        server.use(restify.plugins.bodyParser({ mapParams: true }));
        server.use(helmet());
        server.use(hide({ setTo: '<%= projectName %>' }));   
        
        walk('./lib/api').forEach((file) => {
            var api = require('./api/' + file);
            apis.push(api);

            log.info("Added API version ",api.version());

            server.get({path: "/", version: api.version()}, api.root);
            server.post({path: "/hello", version: api.version()}, api.hello);
        });        
    },
    start: () => {
        log.info("Listening on port ", config.http.port);
        server.listen(config.http.port);

        log.debug("Try sending a request : curl -X POST -H 'Content-type: application/json' -d '{\"name\":\"Client\"}' http://localhost:8080/hello")
    }
};