"use strict";

const bunyan = require("bunyan");
const MongoClient = require('mongodb').MongoClient;

var connection;
var client;

const self = module.exports = {
    getLogger : (moduleName) => {
        return bunyan.createLogger({
            name: moduleName,
            src: false,
            streams: [
                {
                    level:'trace',
                    stream: process.stdout
                },
                {
                    level: 'debug',
                    path: '/tmp/' + moduleName + ".log"
                },
                {
                    level: 'error',
                    path: '/tmp/' + moduleName + ".err"
                }
              ]
        });
    },
    db : {
        connect: (config) => {
            client = new MongoClient(config.url);

            client.connect(function(err) {
              connection = client.db(config.db);
            });
        },
        disconnect: () => {
            if(client) {
                client.close();
            }
        },
        getConnection: () => {
            //This method returns a connection to a db to allow a Data Access Object to fetch collections, perform inserts, and so on ..
            //Refer to http://mongodb.github.io/node-mongodb-native/3.4/quick-start/quick-start/
            return connection;
        }
    }
};