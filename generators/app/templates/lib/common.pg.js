"use strict";

const bunyan = require("bunyan");
const { Pool, Client } = require('pg');

var connection;
var pool;

const self = module.exports = {
    getLogger : (moduleName) => {
        return bunyan.createLogger({
            name: moduleName,
            src: false,
            streams: [
                {
                    level:'trace',
                    stream: process.stdout
                },
                {
                    level: 'debug',
                    path: '/tmp/' + moduleName + ".log"
                },
                {
                    level: 'error',
                    path: '/tmp/' + moduleName + ".err"
                }
              ]
        });
    },
    db : {
        connect: (config) => {
            pool = new Pool({
                user: config.db.user,
                host: config.db.host,
                database: config.db.db,
                password: config.db.password,
                port: config.db.port,
            });
            pool.on('error', (err, client) => {
                throw err;
            });
        },
        disconnect: () => {
            pool.end().then(() => {

            });
        },
        query: (query, args) => {
            return pool.query({
                text: query,
                values: args
            });       
        }
    }
};