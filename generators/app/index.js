'use strict';
const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');

module.exports = class extends Generator {
  prompting() {
    // Have Yeoman greet the user.
    this.log(
      yosay(`Welcome to the neat ${chalk.red('restify-backend')} generator!`)
    );

    const prompts = [
      {
        type: "input",
        name: "projectName",
        message: "Project Name",
        default: "MyAwesomeProject"
      },
      {
        type: "input",
        name: "projectDesc",
        message: "Project description",
        default: "This is my awesome project"
      },
      {
        type: "input",
        name: "projectAuthor",
        message: "Project author",
        default: "nobody"
      },
      {
        type: 'input',
        name: 'httpPort',
        message: 'Listening port for HTTP server (default 8080)?',
        default: 8080
      },
      {
        type: 'list',
        name: 'dbType',
        message: 'Database type [mysql|pgsql|mongodb]?',
        choices: [
          { name: "MySQL", value: "mysql" },
          { name: "MongoDB", value: "mongodb" },
          { name: "PostgreSQL", value:"pg" },
        ]
       }
    ];

    return this.prompt(prompts).then(props => {
      // To access props later use this.props.someAnswer;
      this.props = props;
    });
  }

  writing() {

    let fromCommonFile ;
    let fromConfigFile ;

    if(this.props.dbType == "mongodb") {
      this.props.dbTypeVersion = "^3.4.0";

      fromCommonFile = "lib/common.mongo.js";
      fromConfigFile = "config/config.mongo.json";

    } else if(this.props.dbType == "mysql") {
      this.props.dbTypeVersion = "^2.17.1";

      fromCommonFile = "lib/common.mysql.js";
      fromConfigFile = "config/config.mysql.json";      

    } else if(this.props.dbType == "pg") {
      this.props.dbTypeVersion = "^7.4.1"; 

      fromCommonFile = "lib/common.pg.js";
      fromConfigFile = "config/config.pg.json";    
    }

    this.fs.copyTpl(
      this.templatePath(fromCommonFile),
      this.destinationPath('lib/common.js'),
      this.props
    );          

    this.fs.copyTpl(
      this.templatePath(fromConfigFile),
      this.destinationPath('config/config.json'),
      this.props
    );        

    this.fs.copyTpl(
      this.templatePath('lib/api/1.0.0.js'),
      this.destinationPath('lib/api/1.0.0.js'),
      this.props
    );  

    this.fs.copyTpl(
      this.templatePath('lib/http.js'),
      this.destinationPath('lib/http.js'),
      this.props
    );  

    this.fs.copyTpl(
      this.templatePath('index.js'),
      this.destinationPath('index.js'),
      this.props
    );  

    this.fs.copyTpl(
      this.templatePath('package.json'),
      this.destinationPath('package.json'),
      this.props
    ); 


    this.fs.copyTpl(
      this.templatePath('test/test.js'),
      this.destinationPath('test/test.js'),
      this.props
    );     
    
    this.log(
      yosay(chalk.bold(`Remember to update your ${chalk.yellow("config/config.json")} file with the proper DB params.`))
    )
  }

  install() {
    this.installDependencies({ bower: false });
  }

  end() {
    this.log(
      yosay(chalk.bold(`Let's start our new server running ${chalk.green("node index.js  | bunyan -L")}`))
    );
  }
};
