## Yeoman restify-backend generator

This yeoman generator allows you to create a boilerplate project that uses restify as a REST server, bunyan as a logger, and mysql, mongodb or postgresql helper.

### Usage

To create a quick project you have to install yeoman :

```bash
$ npm install -g yeoman
```

after this, you have to create a directory where the new project will be created:

```bash
$ mkdir myproject && cd $_
```

... and then create the project using the yeoman helper

```bash
$  yo restify-backend
```

You have to provide the needed information as the generator ask you.

This will create the following structure

* **lib/common.js** : The helper methods to create a logger, a database connection and a query method (except for mongodb).
* **config/config.json** : The configuration file that you have to customize and extend as you need.
* **index.js** : The server entry point.
* **lib/http.js** : The restify server that has the API end points for your application; this file has the reference to the API versions you can has (version 1.0.0.js in this case); of course, you can add more versions and you can serve as many versions as you need in the same server.
* **api/1.0.0.js** : The logic that you are serving. You have to add methods as you need and configure those in http.js.
* **package.json** : And, the dependencies file.

You can use nodemon to run your server to restart it as you change your code.

